# WFID

# Server configuration

## Hardware

CPU Details (4x):

```bash
processor       : 0
vendor_id       : AuthenticAMD
cpu family      : 16
model           : 9
model name      : AMD Opteron(tm) Processor 6172
stepping        : 1
microcode       : 0x10000d9
cpu MHz         : 2099.888
cache size      : 512 KB
physical id     : 0
siblings        : 2
core id         : 0
cpu cores       : 2
apicid          : 0
initial apicid  : 0
fpu             : yes
fpu_exception   : yes
cpuid level     : 5
wp              : yes
flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp lm 3dnowext 3dnow constant_tsc art rep_good nopl tsc_reliable nonstop_tsc pni cx16 x2apic popcnt hypervisor lahf_lm cmp_legacy extapic cr8_legacy abm sse4a misalignsse 3dnowprefetch osvw
bogomips        : 4200.05
TLB size        : 1024 4K pages
clflush size    : 64
cache_alignment : 64
address sizes   : 42 bits physical, 48 bits virtual
power management:
```

Memory information:

```bash
# dmidecode 3.0
Scanning /dev/mem for entry point.
SMBIOS 2.4 present.

Handle 0x01A3, DMI type 17, 27 bytes
Memory Device
    Array Handle: 0x01A2
    Error Information Handle: No Error
    Total Width: 32 bits
    Data Width: 32 bits
    Size: 8192 MB
    Form Factor: DIMM
    Set: None
    Locator: RAM slot #0
    Bank Locator: RAM slot #0
    Type: DRAM
    Type Detail: EDO
    Speed: Unknown
    Manufacturer: Not Specified
    Serial Number: Not Specified
    Asset Tag: Not Specified
    Part Number: Not Specified
```

## Software

OS details:

```bash
NAME="CentOS Linux"
VERSION="7 (Core)"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="7"
PRETTY_NAME="CentOS Linux 7 (Core)"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:7"
HOME_URL="https://www.centos.org/"
BUG_REPORT_URL="https://bugs.centos.org/"

CENTOS_MANTISBT_PROJECT="CentOS-7"
CENTOS_MANTISBT_PROJECT_VERSION="7"
REDHAT_SUPPORT_PRODUCT="centos"
REDHAT_SUPPORT_PRODUCT_VERSION="7"
```

yum info:

```bash
3.4.3
  Installed: rpm-4.11.3-43.el7.x86_64 at 2020-09-10 20:56
  Built    : CentOS BuildSystem <http://bugs.centos.org> at 2020-04-01 04:21
  Committed: Panu Matilainen <pmatilai@redhat.com> at 2019-10-04

  Installed: yum-3.4.3-167.el7.centos.noarch at 2020-09-10 20:56
  Built    : CentOS BuildSystem <http://bugs.centos.org> at 2020-04-02 15:56
  Committed: CentOS Sources <bugs@centos.org> at 2020-03-31

  Installed: yum-plugin-fastestmirror-1.1.31-54.el7_8.noarch at 2020-09-10 20:56
  Built    : CentOS BuildSystem <http://bugs.centos.org> at 2020-05-12 16:27
  Committed: Michal Domonkos <mdomonko@redhat.com> at 2020-03-12
```

Apache info:

```bash
Server version: Apache/2.4.6 (CentOS)
Server built:   Apr  2 2020 13:13:23
```

PostgreSQL version: `12.4`

PHP info: `PHP 7.3.22 (cli) (built: Sep  1 2020 09:44:58) ( NTS )`

Drupal info:

```bash
 Drupal version                  :  7.72
 Site URI                        :  http://default
 Database driver                 :  pgsql
 Database hostname               :  localhost
 Database port                   :
 Database username               :  drupal
 Database name                   :  drupal
 Database                        :  Connected
 Drupal bootstrap                :  Successful
 Drupal user                     :
 Default theme                   :  bartik
 Administration theme            :  seven
 PHP configuration               :  /etc/php.ini
 PHP OS                          :  Linux
 Drush script                    :  /usr/local/bin/drush
 Drush version                   :  8.3.2
 Drush temp directory            :  /tmp
 Drush configuration             :
 Drush alias files               :
 Install profile                 :  standard
 Drupal root                     :  /var/www/html
 Drupal Settings File            :  sites/default/settings.php
 Site path                       :  sites/default
 File directory path             :  sites/default/files
 Temporary file directory path   :  /tmp
```

Tripal version: `7.x-3.1`
